//
//  ModelController.m
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "ModelController.h"
#import "DataViewController.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "DataParty/DataParty-Swift.h"
#import "DataList.h"

/*
@property (nonatomic) ller object that manages a simple model -- a collection.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */


@interface ModelController ()

@property (readonly, strong, nonatomic) NSMutableArray *pageData;
@end

@implementation ModelController

- (instancetype)init {
    self = [super init];
    if (self) {
        
        // Create the data model.
        _pageData = [[NSMutableArray alloc] init];
    
        // Add the field map
        [_pageData addObject:@"Field Map"];
        
        //Initialize DataParty
        if (!self.dataPartyEnabled)
        {
            self.dataParty = [[DataParty alloc] initWithSessionId:@"URND" autostart:true];
            self.dataPartyStarted = self.dataParty.isStarted;
            self.dataPartyEnabled = true;
            self.dataParty.delegate = self;
        }
                
    }
    return self;
}

-(int)getNumberOfPages
{
    return [_pageData count];
}

-(NSString*)GetMyParentOutcropName:(NSString*)myName;
{
    int index = 0;
    
    // Find where I am located in the array
    for (NSString* entityName in _pageData)
    {
        if ([entityName isEqualToString:myName])
            break;
        index++;
    }
    
    // From my location in the array, count backwards until you hit an outcrop name
    for (int i = index; index >= 0; i--)
    {
        NSString* name = (NSString*)_pageData[i];
        if ([name containsString:@"Outcrop"])
            return name;
    }
    
    // Return that outcrop name
    return @"Could Not Find Parent Outcrop Object";
}


-(void)AddOutcrop:(DataViewController*)vc
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSString* myName = vc.username;
    NSString* expeditionTitle = vc.expedition;
    
    
    NSString* OutcropName = [[NSString alloc] initWithFormat:@"Outcrop "];
    OutcropName = [OutcropName stringByAppendingString:expeditionTitle];
    OutcropName = [OutcropName stringByAppendingString:@" "];
    OutcropName = [OutcropName stringByAppendingString:myName];
    OutcropName = [OutcropName stringByAppendingString:@" "];
    OutcropName = [OutcropName stringByAppendingString:[dateFormatter stringFromDate:[NSDate date]]];
    
    [_pageData addObject:OutcropName];
}

-(void)LoadOutcrop:(OutcropEntity*)o;
{
    // Load the outcrop in the list ofpages
    [_pageData addObject:o.outcropName];
}

-(void)LoadSample:(SampleEntity*)s
{
    // Load the outcrop in the list ofpages
    [_pageData addObject:s.sampleName];
}

-(void)AddSample:(DataViewController*)vc
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSString* myName = vc.username;
    NSString* expeditionTitle = vc.expedition;
    
    NSString* SampleName = [[NSString alloc] initWithFormat:@"Sample "];
    SampleName = [SampleName stringByAppendingString:expeditionTitle];
    SampleName = [SampleName stringByAppendingString:@" "];
    SampleName = [SampleName stringByAppendingString:myName];
    SampleName = [SampleName stringByAppendingString:@" "];
    SampleName = [SampleName stringByAppendingString:[dateFormatter stringFromDate:[NSDate date]]];
     
    [_pageData addObject:SampleName];
}

-(void)AddSubSample:(DataViewController*)vc
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSString* myName = vc.username;
    NSString* expeditionTitle = vc.expedition;
    
    NSString* SubSampleName = [[NSString alloc] initWithFormat:@"Sub-Sample "];
    SubSampleName = [SubSampleName stringByAppendingString:expeditionTitle];
    SubSampleName = [SubSampleName stringByAppendingString:@" "];
    SubSampleName = [SubSampleName stringByAppendingString:myName];
    SubSampleName = [SubSampleName stringByAppendingString:@" "];

    SubSampleName = [SubSampleName stringByAppendingString:[dateFormatter stringFromDate:[NSDate date]]];
    
    [_pageData addObject:SubSampleName];
}
    
- (DataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard {
    // Return the data view controller for the given index.
    if (([self.pageData count] == 0) || (index >= [self.pageData count])) {
        return nil;
    }

    // Create a new view controller and pass suitable data.
    self.dataViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataViewController"];
    self.dataViewController.dataObject = self.pageData[index];
    self.dataViewController.dataParty = self.dataParty;

    return self.dataViewController;
}

- (NSUInteger)indexOfViewController:(DataViewController *)viewController {
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [self.pageData indexOfObject:viewController.dataObject];
}


#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(DataViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(DataViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageData count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

-(bool) dataReceived:(DataParty *)session data:(DataPartyData *)data
{
    //if session id = URND
    //data is the NSData run through the decoder.
    NSLog(@"URND: Data recieved from DataParty. Unarchiving...");
    DataList *dl = [NSKeyedUnarchiver unarchiveObjectWithData:data.data];
    
    NSLog(@"URND: Data unarchived. Adding to BuddyPins table...");
    //add data to buddy pins table
    //DataViewController *dvc = [[DataViewController alloc]init];
    [self.dataViewController saveBuddyPins:dl.username :[dl.latitude doubleValue] :[dl.longitude doubleValue]];
    
    

    return true;
}

-(void) nearbyPeersChanged:(DataParty *)session nearbyPeers:(NSArray<NSString *> *)nearbyPeers
{
    NSLog(@"NEARBY PEERS CHANGED CALLED FROM JEANS APP.");
}

-(void)connectedPeersChanged:(DataParty *)session connectedPeers:(NSArray<NSString *> *)connectedPeers connectingPeers:(NSArray<NSString *> *)connectingPeers
{
    NSLog(@"CONNECTED PEARS CHANGED FROM JEANS APP");
}

-(void) connectedStateChange:(DataParty *)session isConnected:(BOOL)isConnected
{
    NSLog(@"CONNECTED STATE CHANGED CALLED FROM JEANS APP");
}


@end
