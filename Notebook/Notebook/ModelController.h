//
//  ModelController.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OutcropEntity.h"
#import "SampleEntity.h"
#import "DataParty/DataParty-Swift.h"
#import "DataList.h"

@class DataViewController;

@interface ModelController : NSObject <UIPageViewControllerDataSource, DataPartyDelegate>


- (DataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(DataViewController *)viewController;


-(void)AddOutcrop:(DataViewController*)startingViewController;
-(void)AddSample:(DataViewController*)startingViewController;
-(void)AddSubSample:(DataViewController*)startingViewController;

// Load from HDD
-(void)LoadOutcrop:(OutcropEntity*)o;
-(void)LoadSample:(SampleEntity*)s;


-(int)getNumberOfPages;
-(NSString*)GetMyParentOutcropName:(NSString*)myName;

-(bool)dataReceived:(DataParty *)session data:(DataPartyData *)data;
-(void)nearbyPeersChanged:(DataParty *)session nearbyPeers:(NSArray<NSString *> *)nearbyPeers;
-(void)connectedPeersChanged:(DataParty *)session connectedPeers:(NSArray<NSString *> *)connectedPeers connectingPeers:(NSArray<NSString *> *)connectingPeers;
-(void)connectedStateChange:(DataParty *)session isConnected:(BOOL)isConnected;


@property (strong, nonatomic) DataParty* dataParty;
@property (strong, nonatomic)  DataViewController *dataViewController;
@property bool dataPartyStarted;
@property bool dataPartyEnabled;


@end

