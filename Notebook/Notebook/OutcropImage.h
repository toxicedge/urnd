//
//  OutcropImage.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OutcropEntity;

NS_ASSUME_NONNULL_BEGIN

@interface OutcropImage : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "OutcropImage+CoreDataProperties.h"
