//
//  OutcropEntity.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class OutcropImage, SampleEntity;

NS_ASSUME_NONNULL_BEGIN

@interface OutcropEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "OutcropEntity+CoreDataProperties.h"
