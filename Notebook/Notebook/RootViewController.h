//
//  RootViewController.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OutcropEntity.h"
#import "SampleEntity.h"

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@property NSMutableArray* sampleLocations;

@property int pageNumAfterGlossary; //

-(void) loadOutcrop:(OutcropEntity*) o;
-(void) loadSample:(SampleEntity*) s;

-(int) getNumberOfPages;
-(NSString*)GetMyParentOutcropName:(NSString*)myName;

-(void)setSplLocations:(NSMutableArray*)locations;
-(NSMutableArray*)getSplLocations;

-(void)setPageOnDismissingGlossary:(int)num;
-(int)getPageOnDismissingGlossary;

@end

