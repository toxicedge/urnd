//
//  SubSampleEntity.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SampleEntity;

NS_ASSUME_NONNULL_BEGIN

@interface SubSampleEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "SubSampleEntity+CoreDataProperties.h"
