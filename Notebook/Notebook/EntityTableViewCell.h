//
//  EntityTableViewCell.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-28.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntityTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblEntity;

@end
