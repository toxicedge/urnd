//
//  SampleOnOutcropViewController.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-22.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SampleOnOutcropViewController : UIViewController
<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>


@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *m_crosshair;
@property int idx;

@property (nonatomic,strong) NSMutableArray *images;
@property (nonatomic,strong) NSMutableArray *sampleLocations;

-(void) setImages:(NSMutableArray*)images;
-(NSMutableArray*)getSampleLocations;


@end
