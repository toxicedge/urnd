//
//  main.m
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
