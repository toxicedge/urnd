//
//  OutcropImage+CoreDataProperties.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OutcropImage.h"

NS_ASSUME_NONNULL_BEGIN

@interface OutcropImage (CoreDataProperties)

@property (nullable, nonatomic, retain) NSData *outcropImage;
@property (nullable, nonatomic, retain) OutcropEntity *outcrop;

@end

NS_ASSUME_NONNULL_END
