//
//  DataViewController.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <AVFoundation/AVFoundation.h>
#import "DataList.h"
#import "DataParty/DataParty-Swift.h"


@interface DataViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate,UITextViewDelegate>


@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@property (weak, nonatomic) IBOutlet UIButton *glossaryButton;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UIButton *RFID;
@property (weak, nonatomic) IBOutlet UIButton *pdfInfoButton;

@property (weak, nonatomic) IBOutlet UIButton *m_addOutcropButton;
@property (weak, nonatomic) IBOutlet UIButton *m_backToFieldMap;
@property (weak, nonatomic) IBOutlet UIImageView *m_stamp;

@property (weak, nonatomic) IBOutlet UIScrollView *m_imageScrollView;
@property (weak, nonatomic) IBOutlet UIButton *m_syncButton;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

// The locations of the samples in pixels on each outcrop image.
@property NSMutableArray* sampleLocations;

@property (weak, nonatomic) IBOutlet UITextView *m_text;
@property (weak, nonatomic) IBOutlet UIButton *m_audioButton;
@property (weak, nonatomic) IBOutlet UIButton *m_locationButton;
@property (weak, nonatomic) IBOutlet UIButton *m_deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *m_newSampleButton;
@property (weak, nonatomic) IBOutlet UIButton *trueHeadingButton;
@property (weak, nonatomic) IBOutlet UIButton *magHeadingButton;
@property (weak, nonatomic) IBOutlet UIButton *m_subSampleButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

@property UIImage* m_placeholder;
@property NSMutableArray* m_images;
@property NSString* sampleID;

@property AVAudioRecorder* recorder;
@property NSTimer* timer;
@property AVAudioPlayer* player;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property NSString* m_currentMagHeading;
@property NSString* m_currentTrueHeading;

@property NSString* username;
@property NSString* expedition;
@property NSString* date;

@property (strong, nonatomic) DataParty* dataParty;
@property bool dataPartyStarted;
@property bool dataPartyEnabled;
@property NSString* bpOutcropName;
@property float     bpLatitude;
@property float     bpLongtitude;
@property MKPointAnnotation* bpPoint;
@property CLLocationCoordinate2D bpLocation;


@property (weak, nonatomic) IBOutlet UILabel *version;

@property (weak, nonatomic) IBOutlet UIButton *save;

@property (weak, nonatomic) IBOutlet UIView *m_minimapContainer;

@property (weak, nonatomic) IBOutlet UIButton *outcropIcon;


// Used for Core-Data
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel; // Schema
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;  // Get insert delete from the databse
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;  // Database connection
- (NSURL *)applicationDocumentsDirectory; // nice to have to reference files for core data

-(void)archive;  // Save outcrop names to HDD
-(void)autoload; // Load outcrop names from HDD
-(void)deepLoad; // Load one outcrop's actual data to display in the current view controller
-(void)saveBuddyPins:(NSString*)outcropName :(double)lat :(double)lon;

@end

