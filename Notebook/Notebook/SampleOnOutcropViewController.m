//
//  SampleOnOutcropViewController.m
//  Notebook
//
//  Created by Jean Filion on 2016-01-22.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "SampleOnOutcropViewController.h"
#import "CollectionViewCell.h"
#import "RootViewController.h"
#import "AppDelegate.h"

@interface SampleOnOutcropViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *largeImageView;

@end

@implementation SampleOnOutcropViewController


-(void) setImages:(NSMutableArray*)images
{
    _images = images;
}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ([_images count]);
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
    
    cell.image.image = (UIImage*)[_images objectAtIndex:indexPath.row];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(150.0,150.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _largeImageView.image = (UIImage*)[_images objectAtIndex:indexPath.row];
    
    _idx = indexPath.row;
    
    NSValue *curValue = [_sampleLocations objectAtIndex:_idx];
    CGPoint p = [curValue CGPointValue];
    
    _m_crosshair.center = p;
    
    if ((p.x == 0) && (p.y == 0))
        [_m_crosshair setHidden:true];
    else
        [_m_crosshair setHidden:false];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _idx = 0;
    
    // Create a tap gesture
    _largeImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchedImage:)];
    [_largeImageView addGestureRecognizer:tap];
    
   // _largeImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    _sampleLocations = [[NSMutableArray alloc] init];
    for (int i = 0; i < _images.count; i++)
    {
        CGPoint p = CGPointMake(0.0, 0.0);
        [_sampleLocations addObject:[NSValue valueWithCGPoint:p]];
    }

    [_m_crosshair setHidden:true];
}


- (IBAction)clear:(id)sender {
    NSValue *curValue = [NSValue valueWithCGPoint:CGPointMake(0.0, 0.0)];
    
    [_sampleLocations replaceObjectAtIndex:_idx withObject:curValue];
    
    [_m_crosshair setHidden:true];
}


-(NSMutableArray*)getSampleLocations
{
    return _sampleLocations;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)DoneButton:(id)sender {
    [self dismissViewControllerAnimated:false completion:nil];
}



- (void)touchedImage:(UITapGestureRecognizer *)gesture {
    // When the gesture has ended, perform your action.
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Touched Image");
        
        if (_largeImageView.image == nil)
        {
            [_m_crosshair setHidden:true];
            return;
        }
        
        // Draw crosshairs where user touched.
        CGPoint p = [gesture locationInView:self.view];
        
        // Get a pointer to the current location
        NSValue *curValue = [_sampleLocations objectAtIndex:_idx];
        curValue = [NSValue valueWithCGPoint:p];
        
        [_sampleLocations replaceObjectAtIndex:_idx withObject:curValue];
        
        RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
        [rootController setSplLocations:_sampleLocations];
        
        
        _m_crosshair.center = p;
        
        if ((p.x == 0) && (p.y == 0))
            [_m_crosshair setHidden:true];
        else
            [_m_crosshair setHidden:false];
        
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
