//
//  SubSampleEntity+CoreDataProperties.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SubSampleEntity+CoreDataProperties.h"

@implementation SubSampleEntity (CoreDataProperties)

@dynamic date;
@dynamic expedition;
@dynamic subsampleAudioRecording;
@dynamic subSampleDateTimeOfCollection;
@dynamic subsampleImages;
@dynamic subsampleLocation;
@dynamic subsampleName;
@dynamic subsampleNotes;
@dynamic subsampleParent;
@dynamic subsamplePixelCorrdinatesOnSample;
@dynamic subsampleRFID;
@dynamic username;
@dynamic sample;

@end
