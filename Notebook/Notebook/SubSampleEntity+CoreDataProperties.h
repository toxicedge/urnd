//
//  SubSampleEntity+CoreDataProperties.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SubSampleEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubSampleEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSString *expedition;
@property (nullable, nonatomic, retain) NSData *subsampleAudioRecording;
@property (nullable, nonatomic, retain) NSString *subSampleDateTimeOfCollection;
@property (nullable, nonatomic, retain) NSData *subsampleImages;
@property (nullable, nonatomic, retain) NSString *subsampleLocation;
@property (nullable, nonatomic, retain) NSString *subsampleName;
@property (nullable, nonatomic, retain) NSString *subsampleNotes;
@property (nullable, nonatomic, retain) NSString *subsampleParent;
@property (nullable, nonatomic, retain) NSString *subsamplePixelCorrdinatesOnSample;
@property (nullable, nonatomic, retain) NSString *subsampleRFID;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) SampleEntity *sample;

@end

NS_ASSUME_NONNULL_END
