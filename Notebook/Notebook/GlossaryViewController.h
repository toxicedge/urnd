//
//  GlossaryViewController.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-28.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlossaryViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *glossaryTable;
@property NSMutableArray *entities;
-(void)setListOfEntities:(NSMutableArray*)entities;

@end
