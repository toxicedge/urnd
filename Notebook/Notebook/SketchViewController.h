//
//  SketchViewController.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-23.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SketchViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *penButton;
@property (weak, nonatomic) IBOutlet UIButton *sharpieButton;
@property (weak, nonatomic) IBOutlet UIButton *eraserButton;
@property (weak, nonatomic) IBOutlet UIButton *colorsButton;
@property (weak, nonatomic) IBOutlet UIButton *textButton;
@property (weak, nonatomic) IBOutlet UIButton *clearScreenButton;
@property (weak, nonatomic) IBOutlet UIButton *info;

@property (nonatomic, copy) void (^somethingHappenedInModalVC)(UIImage *sketch);

- (UIImage *) imageWithView:(UIView *)view;
- (void) setBackground:(UIImage*)img;
- (void) archiveExpedition;

@end
