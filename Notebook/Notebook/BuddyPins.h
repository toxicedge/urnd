//
//  BuddyPins.h
//  Notebook
//
//  Created by Student on 2016-03-25.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuddyPins : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "BuddyPins+CoreDataProperties.h"
