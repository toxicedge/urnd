//
//  GlossaryViewController.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-28.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "GlossaryViewController.h"
#import "EntityTableViewCell.h"
#import "Entity.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "DataViewController.h"

@interface GlossaryViewController ()

@end

@implementation GlossaryViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _glossaryTable.dataSource = self;
    _glossaryTable.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setListOfEntities:(NSMutableArray*)entities
{
    _entities = [[NSMutableArray alloc] init];
    _entities = entities.copy;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = [_entities count];
    return [_entities count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EntityTableViewCell *cell = (EntityTableViewCell *)[_glossaryTable dequeueReusableCellWithIdentifier:@"EntityTableViewCell"];
    
    Entity *e = (_entities)[indexPath.row];
    NSString* str = [e description];
    cell.lblEntity.text = str;
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString* entity = [_entities objectAtIndex:indexPath.row];
    
        // num has to be returned to the parent DataViewController
        NSNumber *num = [NSNumber numberWithInt:indexPath.row + 1];
        
        RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
        [rootController setPageOnDismissingGlossary:num.intValue];
    
        [self dismissViewControllerAnimated:NO completion:nil];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
