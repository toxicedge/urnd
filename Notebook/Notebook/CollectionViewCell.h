//
//  CollectionViewCell.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-22.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
