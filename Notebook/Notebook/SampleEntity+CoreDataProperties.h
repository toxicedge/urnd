//
//  SampleEntity+CoreDataProperties.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SampleEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface SampleEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSString *expedition;
@property (nullable, nonatomic, retain) NSString *sampleAudioRecording;
@property (nullable, nonatomic, retain) NSString *sampleDateTimeOfCollection;
@property (nullable, nonatomic, retain) NSString *sampleHeadingMagnetic;
@property (nullable, nonatomic, retain) NSString *sampleHeadingTrue;
@property (nullable, nonatomic, retain) NSData *sampleImages;
@property (nullable, nonatomic, retain) NSString *sampleLocation;
@property (nullable, nonatomic, retain) NSString *sampleName;
@property (nullable, nonatomic, retain) NSString *sampleNotes;
@property (nullable, nonatomic, retain) NSString *sampleParentOutcropName;
@property (nullable, nonatomic, retain) NSData *samplePixelCoordinatesOnOutcropImages;
@property (nullable, nonatomic, retain) NSString *sampleRFID;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSOrderedSet<SampleImage *> *images;
@property (nullable, nonatomic, retain) OutcropEntity *outcrop;
@property (nullable, nonatomic, retain) NSSet<SubSampleEntity *> *subsamples;

@end

@interface SampleEntity (CoreDataGeneratedAccessors)

- (void)insertObject:(SampleImage *)value inImagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromImagesAtIndex:(NSUInteger)idx;
- (void)insertImages:(NSArray<SampleImage *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeImagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInImagesAtIndex:(NSUInteger)idx withObject:(SampleImage *)value;
- (void)replaceImagesAtIndexes:(NSIndexSet *)indexes withImages:(NSArray<SampleImage *> *)values;
- (void)addImagesObject:(SampleImage *)value;
- (void)removeImagesObject:(SampleImage *)value;
- (void)addImages:(NSOrderedSet<SampleImage *> *)values;
- (void)removeImages:(NSOrderedSet<SampleImage *> *)values;

- (void)addSubsamplesObject:(SubSampleEntity *)value;
- (void)removeSubsamplesObject:(SubSampleEntity *)value;
- (void)addSubsamples:(NSSet<SubSampleEntity *> *)values;
- (void)removeSubsamples:(NSSet<SubSampleEntity *> *)values;

@end

NS_ASSUME_NONNULL_END
