//
//  SampleEntity.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "SampleEntity.h"
#import "OutcropEntity.h"
#import "SampleImage.h"
#import "SubSampleEntity.h"

@implementation SampleEntity

// Insert code here to add functionality to your managed object subclass

@end
