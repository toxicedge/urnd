//
//  OutcropEntity+CoreDataProperties.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OutcropEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface OutcropEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *date;
@property (nullable, nonatomic, retain) NSString *expedition;
@property (nullable, nonatomic, retain) NSString *outcropAudioRecording;
@property (nullable, nonatomic, retain) NSString *outcropHeadingMagnetic;
@property (nullable, nonatomic, retain) NSString *outcropHeadingTrue;
@property (nullable, nonatomic, retain) NSString *outcropLocation;
@property (nullable, nonatomic, retain) NSString *outcropName;
@property (nullable, nonatomic, retain) NSString *outcropNotes;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSOrderedSet<OutcropImage *> *images;
@property (nullable, nonatomic, retain) NSSet<SampleEntity *> *samples;

@end

@interface OutcropEntity (CoreDataGeneratedAccessors)

- (void)insertObject:(OutcropImage *)value inImagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromImagesAtIndex:(NSUInteger)idx;
- (void)insertImages:(NSArray<OutcropImage *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeImagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInImagesAtIndex:(NSUInteger)idx withObject:(OutcropImage *)value;
- (void)replaceImagesAtIndexes:(NSIndexSet *)indexes withImages:(NSArray<OutcropImage *> *)values;
- (void)addImagesObject:(OutcropImage *)value;
- (void)removeImagesObject:(OutcropImage *)value;
- (void)addImages:(NSOrderedSet<OutcropImage *> *)values;
- (void)removeImages:(NSOrderedSet<OutcropImage *> *)values;

- (void)addSamplesObject:(SampleEntity *)value;
- (void)removeSamplesObject:(SampleEntity *)value;
- (void)addSamples:(NSSet<SampleEntity *> *)values;
- (void)removeSamples:(NSSet<SampleEntity *> *)values;

@end

NS_ASSUME_NONNULL_END
