//
//  DataList.h
//  Notebook
//
//  Created by Student on 2016-03-15.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DataList : NSObject <NSCoding>
{
    NSString *longitude;
    NSString *latitude;
    NSString *username;
    NSString *date;
    NSString *expiditionName;
}

@property (readwrite) NSString *longitude;
@property (readwrite) NSString *latitude;
@property (readwrite) NSString *username;
@property (readwrite) NSString *date;
@property (readwrite) NSString *expiditionName;

- (NSMutableData*) generateUID;

@end

