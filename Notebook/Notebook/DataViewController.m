//
//  DataViewController.m
//  Notebook
//
//  Created by Jean Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "DataViewController.h"
#import "ModelController.h"
#import "SampleOnOutcropViewController.h"
#import "SketchViewController.h"
#import "ReaderViewController.h"
#import "OutcropEntity.h"
#import "OutcropImage.h"
#import "SampleEntity.h"
#import "SubSampleEntity.h"
#import "BuddyPins.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "SampleImage.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import "GlossaryViewController.h"
#import "DataParty/DataParty-Swift.h"




@interface DataViewController ()

@end

@implementation DataViewController

// For core-data archiving to HD
@synthesize managedObjectContext = _managedObjectContext;               // Get, insert, delete.
@synthesize managedObjectModel = _managedObjectModel;                   // Schema
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;   // Database connection



- (void)viewDidLoad {
    [super viewDidLoad];
    
 
       // Initialize Parse.
    [Parse setApplicationId:@"gymyoUg3d1EakeF8rCRlZ06aqLuhQdRya2V5DscQ"
                  clientKey:@"3xCsDUL2ATzEF87eTsOUk2NbxirVHp3chryC5Zdv"];
    
    
    _m_placeholder = [UIImage imageNamed:@"placeholder"];
    _m_images = [[NSMutableArray alloc]init];
    _sampleLocations = [[NSMutableArray alloc]init];
    
    [_m_images addObject:_m_placeholder];
    
    NSString* title = [_dataObject description];
    _m_text.text = @"Field Notes for ";
    _m_text.text = [_m_text.text stringByAppendingString:title];
    _m_text.text = [_m_text.text stringByAppendingString:@"\n\n"];
    self.m_text.delegate = self;

    
    _sampleID = @"";
    self.mapView.delegate = self;

    
    // Setup the audio recorder
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               [self dateString],
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:NULL];
    _recorder.delegate = self;
    _recorder.meteringEnabled = YES;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager requestAlwaysAuthorization];
    _locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingHeading];
    [_locationManager startUpdatingLocation];
    
    [self updateLocationButton];
    
    NSString* version = @"Version ";
    version = [version stringByAppendingString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    version = [version stringByAppendingString:@" "];
    _version.text = version;
    
    
    
    
    // Load the username and password from the preferences store
    _username = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    _expedition = [[NSUserDefaults standardUserDefaults] objectForKey:@"expedition"];
    
    [self deepLoad];  // Load the content for this outcrop in full detail.
    
}

- (IBAction)Settings:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change User" message:@"Please your username and  expedition title:" delegate:self cancelButtonTitle:@"Continue" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeDefault;
    alertTextField.placeholder = @"First & Last Name:";
    alertTextField.text = _username;

    UITextField * alertTextField2 = [alert textFieldAtIndex:1];
    alertTextField2.keyboardType = UIKeyboardTypeDefault;
    alertTextField2.secureTextEntry = NO;
    alertTextField2.placeholder = @"Expedition Title";
    alertTextField2.text = _expedition;

    [alert setTag:9];
    [alert show];
    
    [self deepLoad];
}

- (void)hideKeyboard
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder)
                                               to:nil
                                             from:nil
                                         forEvent:nil];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    // Do this if we are in Sample or in Outcrop Mode
    NSString* locationButtonTitle = _m_locationButton.titleLabel.text;
    NSString* locStr = [locationButtonTitle stringByReplacingOccurrencesOfString:@"Location: " withString:@""];
    NSArray *strings = [locStr componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D pinCoordinate;
    pinCoordinate.latitude = [strings[0] doubleValue];
    pinCoordinate.longitude = [strings[1] doubleValue];

    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(pinCoordinate, 100000, 100000);
    
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (IBAction)NewOutcrop:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewOutcrop" object:nil];
    [self archive];
}

- (IBAction)NewSample:(id)sender {
    
    
    NSString* title = [_dataObject description];
    
    NSString* parentOutcropName = title;
    
    if ([title containsString:@"Sample"])
    {
        // Recover my Parent Outcrop Name
        RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
        parentOutcropName = [rootController GetMyParentOutcropName:title];
    }
        

    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    SampleOnOutcropViewController * vc = (SampleOnOutcropViewController*)[storyboard instantiateViewControllerWithIdentifier:@"SampleOnOutcropViewController"];
    
    // Jean - Remember to go back here and make this line of code send the images
    // from the Outcrop, not from the previous sample in this outcrop.
    // You need to implement database first.
    
    // use parentOutcropName to recover the images from the parent outcrop.
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    
    
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"outcropName == %@", parentOutcropName]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            OutcropEntity* o = [results objectAtIndex:0]; // There will only ever by 1 perfect match here.
            NSOrderedSet* images = o.images;
            NSMutableArray* arrayOfUIImages = [[NSMutableArray alloc] init];

            for (OutcropImage* oimg in images)
            {
                UIImage* img = [UIImage imageWithData:oimg.outcropImage];
                [arrayOfUIImages addObject:img];
            }
            
            [vc setImages:arrayOfUIImages];

        }
    
        [self presentViewController:vc animated:YES completion:nil];
    
       NSDictionary *dictionary = [NSDictionary dictionaryWithObject:parentOutcropName forKey:@"sampleParentOutcropName"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewSample" object:nil userInfo:dictionary];
    
    [self archive];
}

- (IBAction)NewSubSample:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NewSubSample" object:nil];
}

- (IBAction)RFIDPushed:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"RFID"
                                                   message: @"Enter RFID Number"
                                                  delegate: self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Ok",nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;

    [alert setTag:8];
    [alert show];
    [self archive];
}

- (IBAction)infoButtonPushed:(id)sender {
    // Launch info pdf scrolling view controller
   
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
    NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    
    NSString *filePath = [pdfs firstObject]; assert(filePath != nil); // Path to first PDF file
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        
        [self presentViewController:readerViewController animated:YES completion:nil];
    }
}


- (IBAction)BackToMap:(id)sender {
    [self archive];
    NSNumber *num = [NSNumber numberWithInt:0];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"H2RAutoPage" object:num];
}

- (IBAction)SampleOnOutcropButtonPressed:(id)sender {
    
    NSString* title = [_dataObject description];
    
    if (([title containsString:@"Sample"]) && !([title containsString:@"Sub-Sample"]))
    {
        NSString * storyboardName = @"Main";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
        SampleOnOutcropViewController * vc = (SampleOnOutcropViewController*)[storyboard instantiateViewControllerWithIdentifier:@"SampleOnOutcropViewController"];
    
        [vc setImages:_m_images];
    
        [self presentViewController:vc animated:YES completion:nil];
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NewSample" object:nil];
        
        _sampleLocations = vc.getSampleLocations;

        [self archive];
    }
}


- (void) updateLocationButton
{
    NSString* lon = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
    NSString* lat = [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
    
    NSString* latlon = @"Location: ";
    latlon = [latlon stringByAppendingString:lat];
    latlon = [latlon stringByAppendingString:@","];
    latlon = [latlon stringByAppendingString:lon];
    
    [_m_locationButton setTitle:latlon forState:UIControlStateNormal];
    [self archive];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.dataLabel.text = [self.dataObject description];
    
    if ((_username == nil) || (_expedition == nil))
    {
        [self Settings:self.view];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    // Teleport to the correct page if the glossary view controller tells us to do so.
    RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
    int page = [rootController getPageOnDismissingGlossary];
    
    if (page != 0)
    {
        NSNumber *num = [NSNumber numberWithInt:page];
        [rootController setPageOnDismissingGlossary:0];
        
        NSNumber* index = [[NSNumber alloc] initWithInt:num.intValue];
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:index forKey:@"index"];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"H2RAutoPage" object:nil userInfo:dictionary];
    }
}



- (IBAction)sync:(id)sender {
  
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;

    NSString* title = [_dataObject description];
    
    // We can only access the sync button from the field map page.
    if ([title containsString:@"Field Map"])
    {
        // Save all outcrops first
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            for (OutcropEntity* o in results)
            {
                NSString* location = [o.outcropLocation stringByReplacingOccurrencesOfString:@"Location: " withString:@""];
                
                NSString* magHeading = [o.outcropHeadingMagnetic stringByReplacingOccurrencesOfString:@"Magnetic Heading: " withString:@""];
                
                NSString* truHeading = [o.outcropHeadingTrue stringByReplacingOccurrencesOfString:@"True Heading: " withString:@""];
                
                NSString* outcropAudioName = [o.outcropAudioRecording lastPathComponent];
                NSString* strURL = [o.outcropAudioRecording stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSURL *url = [NSURL URLWithString:strURL];
                NSData* audioData = [NSData dataWithContentsOfURL:url];
                
                PFFile *audioFile = nil;
                if (audioData != nil)
                    audioFile = [PFFile fileWithName:outcropAudioName data:audioData];
                
                PFObject *outcrop = [PFObject objectWithClassName:@"OutcropEntity"];
                
                if (o.outcropName != nil)
                    [outcrop setObject:o.outcropName forKey:@"outcropName"];
                
                if (o.username != nil)
                    [outcrop setObject:o.username forKey:@"username"];
                
                if (o.expedition != nil)
                    [outcrop setObject:o.expedition forKey:@"expedition"];
                
                if (o.date != nil)
                    [outcrop setObject:o.date forKey:@"date"];
                
                if (o.outcropName != nil)
                    [outcrop setObject:o.outcropNotes forKey:@"outcropNotes"];
                
                if (location != nil)
                    [outcrop setObject:location forKey:@"outcropLocation"];
                
                if (magHeading != nil)
                    [outcrop setObject:magHeading forKey:@"outcropHeadingMagnetic"];
                
                if (truHeading != nil)
                    [outcrop setObject:truHeading forKey:@"outcropHeadingTrue"];
                
                if (audioFile != nil)
                    [outcrop setObject:audioFile forKey:@"outcropAudioRecording"];
                
                int i = 0;
                for (OutcropImage* imageData in o.images)
                {
                    UIImage* img = [UIImage imageWithData:imageData.outcropImage];
                    NSString* outcropImageName = @"OutcropImage";
                    outcropImageName = [outcropImageName stringByAppendingString:[@(i) stringValue]];
                    
                    NSString* outcropImageNameJPG = [outcropImageName stringByAppendingString:@".jpg"];
                    
                    NSData *imgInNParseDataFormat = UIImageJPEGRepresentation(img, 0.05f);
                    PFFile *imgInParseFormat = [PFFile fileWithName:outcropImageNameJPG data:imgInNParseDataFormat];
                    [outcrop setObject:imgInParseFormat forKey:outcropImageName];
                    i++;
                }
                
                NSLog(@"Outcrop uploading to Parse...");
                [outcrop saveInBackground];
            }
            
            
            // Now save all samples
            request = [[NSFetchRequest alloc] init];
            [request setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
            
            predicateArray = [NSMutableArray array];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
            [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
            compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
            [request setPredicate:compoundpred];
            
            results = [context executeFetchRequest:request error:&error];
            if (error)
                NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
            
            
            if (results.count > 0)
            {
                for (SampleEntity* s in results)
                {
                    NSString* location = [s.sampleLocation stringByReplacingOccurrencesOfString:@"Location: " withString:@""];
                    
                    NSString* rfid = [s.sampleRFID stringByReplacingOccurrencesOfString:@"RFID: " withString:@""];
                    
                    if ([rfid isEqualToString:@""])
                        rfid = nil;
                    
                    NSString* magHeading = [s.sampleHeadingMagnetic stringByReplacingOccurrencesOfString:@"Magnetic Heading: " withString:@""];
                    
                    NSString* truHeading = [s.sampleHeadingTrue stringByReplacingOccurrencesOfString:@"True Heading: " withString:@""];
                    
                    NSString* sampleAudioName = [s.sampleAudioRecording lastPathComponent];
                    
                    
                    PFObject *sample = [PFObject objectWithClassName:@"SampleEntity"];
                    
                    if (sampleAudioName != nil)
                    {
                        
                        NSString* strURL = [s.sampleAudioRecording stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                        NSURL *url = [NSURL URLWithString:strURL];
                        NSData* audioData = [NSData dataWithContentsOfURL:url];
                        
                        PFFile *audioFile = nil;
                        
                        if (audioData != nil)
                        {
                            audioFile = [PFFile fileWithName:sampleAudioName data:audioData];
                                                    audioFile = [PFFile fileWithName:sampleAudioName data:audioData];
                            [sample setObject:audioFile forKey:@"sampleAudioRecording"];
                        }

                    }
                    
                    if (s.sampleName != nil)
                        [sample setObject:s.sampleName forKey:@"sampleName"];
                    
                    if (s.username != nil)
                        [sample setObject:s.username forKey:@"username"];
                    
                    if (s.expedition != nil)
                        [sample setObject:s.expedition forKey:@"expedition"];
                    
                    if (s.date != nil)
                        [sample setObject:s.date forKey:@"date"];
                    
                    if (s.sampleNotes != nil)
                        [sample setObject:s.sampleNotes forKey:@"sampleNotes"];
                    
                    if (location != nil)
                        [sample setObject:location forKey:@"sampleLocation"];
                    
                    if (magHeading != nil)
                        [sample setObject:magHeading forKey:@"sampleHeadingMagnetic"];
                    
                    if (truHeading != nil)
                        [sample setObject:truHeading forKey:@"sampleHeadingTrue"];
                    
                    if (s.sampleParentOutcropName != nil)
                        [sample setObject:s.sampleParentOutcropName forKey:@"sampleParentOutcropName"];
                    
                    if (s.sampleDateTimeOfCollection != nil)
                        [sample setObject:s.sampleDateTimeOfCollection forKey:@"sampleDateTimeOfCollection"];
                    
                    if (rfid != nil)
                        [sample setObject:rfid forKey:@"SampleRFID"];
                    
                    // Load sample location on all parent outcrop images.
                    if (s.samplePixelCoordinatesOnOutcropImages != nil)
                    {
                        NSData* locsNSDataFormat = s.samplePixelCoordinatesOnOutcropImages;
                        NSMutableArray* pxlCoords = [NSKeyedUnarchiver unarchiveObjectWithData:locsNSDataFormat];
                    
                        int i = 0;
                        for (NSValue* coords in pxlCoords)
                        {
                            NSString* outcropCoordsName = @"PixelCoordsOnImage";
                            outcropCoordsName = [outcropCoordsName stringByAppendingString: [@(i) stringValue]];
                            
                            CGPoint point = [coords CGPointValue];
                            
                            NSString* strCoords = NSStringFromCGPoint(point);
                            
                            [sample setObject:strCoords forKey:outcropCoordsName];
                            i++;
                        }
                    }

                    
                    int i = 0;
                    for (SampleImage* imageData in s.images)
                    {
                        UIImage* img = [UIImage imageWithData:imageData.sampleImage];
                        NSString* sampleImageName = @"SampleImage";
                        sampleImageName = [sampleImageName stringByAppendingString:[@(i) stringValue]];
                        NSString* sampleImageNameJPG = [sampleImageName stringByAppendingString:@".jpg"];

                        
                        NSData *imgInNParseDataFormat = UIImageJPEGRepresentation(img, 0.05f);
                        
                        PFFile *imgInParseFormat = [PFFile fileWithName:sampleImageNameJPG data:imgInNParseDataFormat];
                        [sample setObject:imgInParseFormat forKey:sampleImageName];
                        i++;
                    }
                    
                    NSLog(@"Sample uploading to Parse...");
                    [sample saveInBackground];
                }

            }
        }
    }
}


-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    [self autoload];  // Try to load data from HDD if there is any.

     NSString* title = [_dataObject description];
    
    // Clear the contents of the scroll view.
    for (UIView *subview in _m_imageScrollView.subviews) {
        [subview removeFromSuperview];
    }
    
    if (![title isEqualToString:@"Field Map"])
    {
        // This is either an Outcrop or a Sample viewcontroller
        
        // Set button visibility
        [_m_backToFieldMap setHidden:false];
        
        [_glossaryButton setHidden:true];
        [_m_addOutcropButton setHidden:true];
        [_m_text setHidden:false];
        [_m_audioButton setHidden:false];
        [_m_locationButton setHidden:false];
        [_magHeadingButton setHidden:false];
        [_trueHeadingButton setHidden:false];
        [_m_syncButton setHidden:true];
        [_m_deleteButton setHidden:false];
        [_m_newSampleButton setHidden:false];
        [_outcropIcon setHidden:false];
        [_m_subSampleButton setHidden:true];
        [_RFID setHidden:true];
        [_settingsButton setHidden:true];
        [_pdfInfoButton setHidden:false];
        [_m_stamp setHidden:true];
        [_save setHidden:false];

        [_outcropIcon setImage:[UIImage imageNamed:@"theoutcrop"] forState:UIControlStateNormal];
        
        if ([title containsString:@"Sub-Sample"])
        {
            [_RFID setHidden:true];
            [_m_subSampleButton setHidden:true];
            [_outcropIcon setImage:[UIImage imageNamed:@"thesubsample"] forState:UIControlStateNormal];
        }
        else if ([title containsString:@"Sample"])
        {
            [_RFID setHidden:false];
            [_m_subSampleButton setHidden:true];
            [_outcropIcon setImage:[UIImage imageNamed:@"thesample"] forState:UIControlStateNormal];
        }
        
        
        // Make the map smaller
        [_map setFrame:_m_minimapContainer.frame];
        [_map setHidden:false];
        
        // Load images for viewing in the scroll view
        NSInteger numberOfViews = _m_images.count;
        
        for (int i = 0; i < numberOfViews; i++)
        {
        
            CGFloat xOrigin = i * _m_imageScrollView.frame.size.width;
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:
                                  CGRectMake(xOrigin, 0,
                                             _m_imageScrollView.frame.size.width,
                                             _m_imageScrollView.frame.size.height)];
            
            image.image = (UIImage*)_m_images[i];
            image.layer.borderColor = [UIColor whiteColor].CGColor;
            image.layer.borderWidth = 1;
            image.userInteractionEnabled = YES;
            
            // Create a tap gesture
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchedImage:)];
            [image addGestureRecognizer:tap];
            
            image.contentMode = UIViewContentModeScaleAspectFit;
            [_m_imageScrollView addSubview:image];
        }
        
        _m_imageScrollView.contentSize = CGSizeMake(_m_imageScrollView.frame.size.width *
                                                 numberOfViews,
                                                _m_imageScrollView.frame.size.height);
        

    }
    else
    {
        // This is the field map
        
        // Set object visibility
        [_glossaryButton setHidden:false];
        [_m_backToFieldMap setHidden:true];
        [_m_addOutcropButton setHidden:false];
        [_m_text setHidden:true];
        [_m_audioButton setHidden:true];
        [_m_locationButton setHidden:true];
        [_magHeadingButton setHidden:true];
        [_trueHeadingButton setHidden:true];
        [_m_deleteButton setHidden:true];
        [_m_newSampleButton setHidden:true];
        [_m_syncButton setHidden:false];
        [_m_subSampleButton setHidden:true];
        [_outcropIcon setHidden:true];
        [_RFID setHidden:true];
        [_settingsButton setHidden:false];
        [_pdfInfoButton setHidden:true];
        [_m_stamp setHidden:false];
        [_save setHidden:true];
    }
    
}


// This will load, only once, the names of all samples, subsamples, and outcrops to creat their
// pages.  It won't load their heavy data though, that will be done just in time to save memory.
-(void)autoload
{
    // Load this user's outcrop, sample, and subsample names only into the list of pages.
    // Their data and content will be loaded on a need to know basis only.
    
    RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];

    int numberOfPages = [rootController getNumberOfPages];
    if (numberOfPages > 1)
        return; // data already loaded, don't do it again.
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    
    [request setPredicate:compoundpred];
    
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error)
        NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
    
    
    for (OutcropEntity *o in results) {
        // Add outcrop from HDD to the list of outcrops in memory.
        [rootController loadOutcrop:o];
        
        
        // Now load all samples that belong to this outcrop.
        request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"sampleParentOutcropName == %@", o.outcropName]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
        
        
        for (SampleEntity *s in results) {
            s.sampleParentOutcropName = o.outcropName;
            // Add Sample from HDD to the list of outcrops in memory.
            [rootController loadSample:s];
        }
        
    }
}

-(void)addPin{
    
    [self.mapView addAnnotation:self.bpPoint];
    [self.mapView selectAnnotation:self.bpPoint animated:YES];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.bpLocation, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    NSError* error;
    bool buddyPin = false;
    annView.enabled = YES;
    annView.canShowCallout = YES;
    
    NSFetchRequest *bpRequest = [[NSFetchRequest alloc] init];
    [bpRequest setEntity:[NSEntityDescription entityForName:@"BuddyPins" inManagedObjectContext:self.managedObjectContext]];
    
    NSArray *bpResults = [self.managedObjectContext executeFetchRequest:bpRequest error:&error];
    if (error)
        NSLog(@"Whoops, couldn't fetch requests on BuddyPins: %@", [error localizedDescription]);
    
    if (bpResults.count > 0)
    {
        // Drop a pin on the map for this BuddyPin
        // Add an annotation
        for (BuddyPins* bp in bpResults)
        {
            if (bp.outcropName == annotation.title)
            {
                buddyPin = true;
            }
            
        }
        
    }


    if(buddyPin)
    {
        annView.pinTintColor = [UIColor purpleColor];
    }else
    {
        annView.pinTintColor = [UIColor redColor];
    }
    return annView;
}

-(void)deepLoad
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    
    NSString* title = [_dataObject description];
    
    //Drop DataParty BuddyPins
    NSFetchRequest *bpRequest = [[NSFetchRequest alloc] init];
    [bpRequest setEntity:[NSEntityDescription entityForName:@"BuddyPins" inManagedObjectContext:context]];
    
    NSArray *bpResults = [context executeFetchRequest:bpRequest error:&error];
    if (error)
        NSLog(@"Whoops, couldn't fetch requests on BuddyPins: %@", [error localizedDescription]);
    
    if (bpResults.count > 0)
    {
        // Drop a pin on the map for this BuddyPin
        // Add an annotation
        for (BuddyPins* bp in bpResults)
        {
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            double lat = [bp.latitude doubleValue];
            double lon = [bp.longtitude doubleValue];
        
            
            CLLocationCoordinate2D pinCoordinate;
            pinCoordinate.latitude = lat;
            pinCoordinate.longitude = lon;
            point.coordinate = pinCoordinate;
            self.bpPoint = point;
            
            point.title = bp.outcropName;
            point.subtitle = [NSString stringWithFormat:@"%f,%f", lat,lon];
            
            self.bpLocation = pinCoordinate;
            //[self.mapView addAnnotation:point];
            //use a slight delay for more dramtic zooming
            [self performSelectorOnMainThread:@selector(addPin) withObject:nil waitUntilDone:YES];
        
        }
        
    }
    
    if ([title containsString:@"Field Map"])
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            for (OutcropEntity* o in results)
            {
                // Drop a pin on the map for this outcrop
                // Add an annotation
                MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                NSString* locStr = o.outcropLocation;
                NSArray *strings = [locStr componentsSeparatedByString:@","];
                
                CLLocationCoordinate2D pinCoordinate;
                pinCoordinate.latitude = [strings[0] doubleValue];
                pinCoordinate.longitude = [strings[1] doubleValue];
                point.coordinate = pinCoordinate;
                
                point.title = o.outcropName;
                point.subtitle = o.outcropLocation;
                
                //Update DataParty
                
                DataList *data = [[DataList alloc] init];
                data.longitude = [[NSString alloc] initWithFormat:@"%f", pinCoordinate.longitude];
                data.latitude = [[NSString alloc] initWithFormat:@"%f", pinCoordinate.latitude];
                data.username = _username;
                data.expiditionName = _expedition;
                data.date = _date;
                
                //[data encodeWithCoder:archive];
                NSData* rawData = [NSKeyedArchiver archivedDataWithRootObject:data];
                
                DataPartyData *dpData = [[DataPartyData alloc] initWithDataType:@"Outcrop" data:rawData dataHash: [data generateUID]];
                
                [_dataParty addData:dpData];
                
                //END DP UPDATE
                
                [self.mapView addAnnotation:point];
                
            }

            
        }
        
        
        request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
        
        results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            for (SampleEntity* s in results)
            {
                // Drop a pin on the map for this outcrop
                // Add an annotation
                MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                NSString* locStr = s.sampleLocation;
                NSArray *strings = [locStr componentsSeparatedByString:@","];
                
                CLLocationCoordinate2D pinCoordinate;
                pinCoordinate.latitude = [strings[0] doubleValue];
                pinCoordinate.longitude = [strings[1] doubleValue];
                point.coordinate = pinCoordinate;
                
                point.title = s.sampleName;
                point.subtitle = s.sampleLocation;
                
                [self.mapView addAnnotation:point];
            }
        }
        
    }
    
    else if ([title containsString:@"Outcrop"])
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"outcropName == %@", title]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            OutcropEntity* o = [results objectAtIndex:0]; // There will only ever by 1 perfect match here.
            NSString* location = o.outcropLocation;
            if (![location containsString:@"Location: "])
            {
                location = @"Location: ";
                location = [location stringByAppendingString:o.outcropLocation];
            }
            
            [_m_locationButton setTitle:location forState:UIControlStateNormal];
            _m_text.text = o.outcropNotes;
            
            NSString* headTrue = o.outcropHeadingTrue;
            NSString* headMag = o.outcropHeadingMagnetic;
            
            _m_currentTrueHeading = o.outcropHeadingTrue;
            _m_currentMagHeading = o.outcropHeadingMagnetic;
            [_magHeadingButton setTitle:headMag forState:UIControlStateNormal];
            [_trueHeadingButton setTitle:headTrue forState:UIControlStateNormal];
            _date = o.date;
            
            NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
            [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
            [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
            [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
            
            NSString* strURL = [o.outcropAudioRecording stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
           
            NSURL *url = [NSURL URLWithString:strURL];
            
            _recorder = [[AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:NULL];

            NSOrderedSet* images = o.images;
            [_m_images removeAllObjects];
            
            int index = 0;
            for (OutcropImage* imageData in images)
            {
                UIImage* img = [UIImage imageWithData:imageData.outcropImage];
                [_m_images insertObject:img atIndex:index];
                index++;
            }
            
            [_m_images addObject:_m_placeholder];

            
            // Drop a pin on the map for this outcrop
            // Add an annotation
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            NSString* locStr = o.outcropLocation;
            NSArray *strings = [locStr componentsSeparatedByString:@","];
            CLLocationCoordinate2D pinCoordinate;
            pinCoordinate.latitude = [strings[0] doubleValue];
            pinCoordinate.longitude = [strings[1] doubleValue];
            point.coordinate = pinCoordinate;
            point.title = o.outcropName;
            point.subtitle = o.outcropLocation;
            [self.mapView addAnnotation:point];

            return;
        }
        
    }
    
    else if ([title containsString:@"SubSample"])
    {
    }
    
    else if ([title containsString:@"Sample"])
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
        
        // Recover my Parent Outcrop Name
        RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
        
        NSString* parentName = [rootController GetMyParentOutcropName:title];
        
        _sampleLocations = [rootController getSplLocations]; // Not sure if I need this.
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"sampleParentOutcropName == %@", parentName]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"sampleName == %@", title]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
        
        
        if (results.count > 0)
        {
            SampleEntity* s = [results objectAtIndex:0]; // There will only ever by 1 perfect match here.
            NSString* location = s.sampleLocation;

            if (![location containsString:@"Location: "])
            {
                location = @"Location: ";
                location = [location stringByAppendingString:s.sampleLocation];
            }

            [_m_locationButton setTitle:location forState:UIControlStateNormal];
            _m_text.text = s.sampleNotes;
            _m_currentTrueHeading = s.sampleHeadingTrue;
            _m_currentMagHeading = s.sampleHeadingMagnetic;
            [_magHeadingButton setTitle:_m_currentMagHeading forState:UIControlStateNormal];
            [_trueHeadingButton setTitle:_m_currentTrueHeading forState:UIControlStateNormal];
            
            NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
            [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
            [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
            [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
            
            _date = s.date;
            NSString* idStr = s.sampleRFID;
            [_RFID setTitle:idStr forState:UIControlStateNormal];
            
            NSString* strURL = [s.sampleAudioRecording stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSURL *url = [NSURL URLWithString:strURL];
            
            _recorder = [[AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:NULL];
            
            // Load sample location on all parent outcrop images.
            NSData* locsNSDataFormat = s.samplePixelCoordinatesOnOutcropImages;
            _sampleLocations = [NSKeyedUnarchiver unarchiveObjectWithData:locsNSDataFormat];
            
            
            NSOrderedSet* images = s.images;
            [_m_images removeAllObjects];
            
            int index = 0;
            for (SampleImage* imageData in images)
            {
                UIImage* img = [UIImage imageWithData:imageData.sampleImage];
                [_m_images insertObject:img atIndex:index];
                index++;
            }
            
            [_m_images addObject:_m_placeholder];

            
            // Drop a pin on the map for this sample
            // Add an annotation
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            NSString* locStr = s.sampleLocation;
            NSArray *strings = [locStr componentsSeparatedByString:@","];
            CLLocationCoordinate2D pinCoordinate;
            pinCoordinate.latitude = [strings[0] doubleValue];
            pinCoordinate.longitude = [strings[1] doubleValue];
            point.coordinate = pinCoordinate;
            point.title = s.sampleName;
            point.subtitle = s.sampleLocation;
            [self.mapView addAnnotation:point];
            
            return;
        }

    }
}

- (void)saveBuddyPins:(NSString*)outcropName :(double)lat :(double)lon
{
   
    NSLog(@"Attempting to add the following data to BuddyPins Table: lat:%f long:%f outcropName: %@", lat,lon, outcropName);
    
    NSNumber *doubleLat = [[NSNumber alloc] initWithDouble:lat];
    NSNumber *doubleLon = [[NSNumber alloc] initWithDouble:lon];
    
    //Check to make sure Buddy Pin doesn't already exists in the BuddyPins table
    //Drop DataParty BuddyPins
    NSError* error = nil;
    bool pinAlreadyExists = false;
    NSFetchRequest *bpRequest = [[NSFetchRequest alloc] init];
    [bpRequest setEntity:[NSEntityDescription entityForName:@"BuddyPins" inManagedObjectContext:self.managedObjectContext]];
    
    NSArray *bpResults = [self.managedObjectContext executeFetchRequest:bpRequest error:&error];
    if (error)
        NSLog(@"Whoops, couldn't fetch requests on BuddyPins: %@", [error localizedDescription]);
    
    if (bpResults.count > 0)
    {
        for (BuddyPins* bp in bpResults)
        {
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            
            [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
            [formatter setMaximumFractionDigits:4];
            [formatter setRoundingMode: NSNumberFormatterRoundUp];
            
            NSString *bplat =[formatter stringFromNumber:bp.latitude];
            NSString *bplon =[formatter stringFromNumber:bp.longtitude];
            NSString *dplat =[formatter stringFromNumber:doubleLat];
            NSString *dplon =[formatter stringFromNumber:doubleLon];
            
            if (bplat == dplat && bplon == dplon)
            {
                pinAlreadyExists = true;
                break;
            }
        }
        
    }
    
    if (!pinAlreadyExists)
    {
        //pin BuddyPin to map
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    
        CLLocationCoordinate2D pinCoordinate;
        pinCoordinate.latitude = lat;
        pinCoordinate.longitude = lon;
        point.coordinate = pinCoordinate;
        
        point.title = outcropName;
        [self performSelector:@selector(refreshMapAnnotations:) withObject:point afterDelay:0.5];
        //[self.mapView addAnnotation:point];
        //refresh coords
     
//        [self.mapView setCenterCoordinate:self.mapView.region.center animated:NO];
//
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"New Outcrop Point"
//                                                       message: [NSString stringWithFormat:@"Recieved outcrop point from %@",outcropName]
//                                                      delegate: self
//                                             cancelButtonTitle:@"OK"
//                                             otherButtonTitles:nil];
//        [alert setTag:9];
//        [alert show];
      
        
        //save buddy pin to database
        NSLog(@"Pin doesnt exist in database. adding Buddy Pin");
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"BuddyPins" inManagedObjectContext:self.managedObjectContext];
        NSManagedObject *newBuddy = [[NSManagedObject alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];

        [newBuddy setValue:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
        [newBuddy setValue:[NSNumber numberWithDouble:lon] forKey:@"longtitude"];
        [newBuddy setValue:outcropName forKey:@"outcropName"];


        if ([self.managedObjectContext save:&error] == NO) {
            NSAssert(NO, @"Error saving BuddyPin context: %@\n%@", [error localizedDescription], [error userInfo]);
        }
        [self deepLoad];
    }
    else
    {
        NSLog(@"Buddy pin coords already exist in URND database. Will not add");
    }
    
}

-(void)refreshMapAnnotations:(MKPointAnnotation*)point
{
    [self.mapView addAnnotation:point];
}

- (IBAction)save:(id)sender {
    [self archive];
    [self hideKeyboard];
}


// This will archive only the current page of deep data.
-(void)archive
{
    // Local Database Update (save to disk if it's a new outcrop, sample, or subsample).
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    
    // To update the contents of the db, I have to fetch the object from the context, change the properties you desire, then save the context.
    
    // First we must discover if this entitiy is an outcrop, a sample, or a subsample.
    NSString* title = [_dataObject description];
    
    if ([title containsString:@"Outcrop"])
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"outcropName == %@", title]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        [request setPredicate:compoundpred];

        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
        
        OutcropEntity* o;
        
        if (results.count > 0)  // If the expedition already exists, update its contents.
        {
            o = [results objectAtIndex:0]; // There will only ever by 1 perfect match here.
        }
        else // If the outcrop does not already exist, create a new one in the database
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            _date = [dateFormatter stringFromDate:[NSDate date]];
            
            o = [NSEntityDescription
                 insertNewObjectForEntityForName:@"OutcropEntity"
                 inManagedObjectContext:context];
        }
        
        NSString* location = _m_locationButton.titleLabel.text;

        [o setValue:_username forKey:@"username"];
        [o setValue:_expedition  forKey:@"expedition"];
        
        
        NSString *stringsWithoutHeader = [location stringByReplacingOccurrencesOfString:@"Location: " withString:@""];
        
        [o setValue:stringsWithoutHeader forKey:@"outcropLocation"];
        [o setValue:title forKey:@"outcropName"];
        [o setValue:_m_text.text forKey:@"outcropNotes"];
        [o setValue:_trueHeadingButton.titleLabel.text forKey:@"outcropHeadingTrue"];
        [o setValue:_magHeadingButton.titleLabel.text forKey:@"outcropHeadingMagnetic"];
        
        NSString* strURL = [_recorder.url absoluteString];
        
        [o setValue:strURL forKey:@"outcropAudioRecording"];
    
        NSMutableOrderedSet* set = [[NSMutableOrderedSet alloc] init];
        
        int last = _m_images.count-1;
        int index = 0;
        for (UIImage* image in _m_images) {
            // For each image that exists in the mutable array,
            // Add an outcropImage object to the CoreData
            
            if (index == last)
                continue;
            
            OutcropImage *i = [NSEntityDescription
                               insertNewObjectForEntityForName:@"OutcropImage"
                               inManagedObjectContext:context];
                
            NSData* img = UIImagePNGRepresentation(image);
            i.outcropImage = img;
            i.outcrop = o;
                
            [set addObject:i];
            index++;
        }
            
        [o setImages:(NSOrderedSet *)set.copy];
        
        
        
        // Save a new expedition OR Update the existing one in the database
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }

    }
    
    else if ([title containsString:@"SubSample"])
    {
        
    }
    
    
    else if ([title containsString:@"Sample"])
    {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
        
        NSMutableArray *predicateArray = [NSMutableArray array];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
        [predicateArray addObject:[NSPredicate predicateWithFormat:@"sampleName == %@", title]];
        NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
        
        [request setPredicate:compoundpred];
        
        NSArray *results = [context executeFetchRequest:request error:&error];
        if (error)
            NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
        
        SampleEntity* s;
        
        if (results.count > 0)  // If the sample already exists, update its contents.
        {
            s = [results objectAtIndex:0]; // There will only ever by 1 perfect match here.
        }
        else // If the sample does not already exist, create a new one in the database
        {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            _date = [dateFormatter stringFromDate:[NSDate date]];
            
            s = [NSEntityDescription
                 insertNewObjectForEntityForName:@"SampleEntity"
                 inManagedObjectContext:context];
        }
        
        NSString* location = _m_locationButton.titleLabel.text;
        NSString*  parentName = @"NotAnOutcrop";

        // Recover my Parent Outcrop Name
        RootViewController* rootController =(RootViewController *)[[(AppDelegate *) [[UIApplication sharedApplication]delegate] window] rootViewController];
        
        parentName = [rootController GetMyParentOutcropName:title];
        
        // Save the locations of this sample on all outcrop images.
        _sampleLocations = [rootController getSplLocations];
        NSData* myNSDataArray = [NSKeyedArchiver archivedDataWithRootObject:_sampleLocations];
        [s setValue:myNSDataArray forKey:@"SamplePixelCoordinatesOnOutcropImages"];
        
        
        NSString* locStr = [location stringByReplacingOccurrencesOfString:@"Location: " withString:@""];
        
        [s setValue:_username forKey:@"username"];
        [s setValue:_expedition  forKey:@"expedition"];
        [s setValue:locStr forKey:@"sampleLocation"];
        [s setValue:title forKey:@"sampleName"];
        [s setValue:_m_text.text forKey:@"sampleNotes"];
        [s setValue:_trueHeadingButton.titleLabel.text forKey:@"sampleHeadingTrue"];
        [s setValue:_magHeadingButton.titleLabel.text forKey:@"sampleHeadingMagnetic"];
        [s setValue:_date forKey:@"sampleDateTimeOfCollection"];
        [s setValue:parentName forKey:@"sampleParentOutcropName"];
        [s setValue:_RFID.titleLabel.text forKey:@"sampleRFID"];
        
        NSString* strURL = [_recorder.url absoluteString];
        [s setValue:strURL forKey:@"sampleAudioRecording"];
        
        NSMutableOrderedSet* set = [[NSMutableOrderedSet alloc] init];

        int last = _m_images.count-1;
        int index = 0;

        for (UIImage* image in _m_images) {
            // For each image that exists in the mutable array,
            // Add a sampleImage object to the CoreData
            
            if (index == last)
                continue;
            
            SampleImage* i = [NSEntityDescription
                               insertNewObjectForEntityForName:@"SampleImage"
                               inManagedObjectContext:context];
            
            NSData* img = UIImagePNGRepresentation(image);
            i.sampleImage = img;
            i.sample = s;
            
            [set addObject:i];
            
            index++;
        }
        
        [s setImages:(NSOrderedSet *)set.copy];
    
        //@property (nullable, nonatomic, retain) OutcropEntity *outcrop;
        //@property (nullable, nonatomic, retain) SubSampleEntity *subsamples;

        
        // Save a new expedition OR Update the existing one in the database
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }

    }


    
}

- (void)touchedImage:(UITapGestureRecognizer *)gesture {
    // When the gesture has ended, perform your action.
    if (gesture.state == UIGestureRecognizerStateEnded) {
        NSLog(@"Touched Image");
        
        // Identify which image is currently selected.
        CGFloat pageWidth = _m_imageScrollView.frame.size.width;
        int page = floor((_m_imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        UIImageView* v = _m_imageScrollView.subviews[page];
        UIImage* img = v.image;
        
        if (img == _m_placeholder)
        {
            NSString* title = [_dataObject description];
            
            if ([title containsString:@"Outcrop"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Add a New Image of this Outcrop"
                                                               message: @"What kind of image?"
                                                              delegate: self
                                                     cancelButtonTitle:@"Cancel"
                                                     otherButtonTitles:@"Photo",@"Sketch",nil];
                [alert setTag:0];
                [alert show];
            }
            else if ([title containsString:@"Sample"])
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Add a New Image of this Sample"
                                                               message: @"What kind of image?"
                                                              delegate: self
                                                     cancelButtonTitle:@"Cancel"
                                                     otherButtonTitles:@"Sample on Outcrop",@"Closeup of Sample",@"Damaged Outcrop",@"Sketch",nil];
                [alert setTag:10];
                [alert show];
            }

            
            
        }
        
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Modify an image of this outcrop"
                                                           message: @"What would you like to do?"
                                                          delegate: self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"Overlay Sketch",@"Delete Image",nil];
            [alert setTag:1];
            [alert show];
        }
    }
}


// Alert View Event Handler
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 0) { // Take New Outcrop Photo Alert View
        if (buttonIndex == 0)
        {
            NSLog(@"cancel");
            // Any action can be performed here
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"photo");
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            NSLog(@"sketch");
            
            // Launch the sketching view controller
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            SketchViewController * vc = (SketchViewController*)[storyboard instantiateViewControllerWithIdentifier:@"SketchViewController"];
            
            // Fancy Block command here.  Nice job, Jean.  It will go here when the image is updated in the other view controller.
            vc.somethingHappenedInModalVC = ^(UIImage *sketch) {
                
                // Add the sketch to the list of images
                [_m_images removeObjectAtIndex:_m_images.count -1];
                [_m_images addObject:sketch];
                [_m_images addObject:_m_placeholder];
                [self.view setNeedsLayout];
                [self.view layoutIfNeeded];
                [self archive];

            };
            
            
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
    
    
    else if (alertView.tag == 1) { // Modify Existing Outcrop Photo Alert View
        if (buttonIndex == 0)
        {
            NSLog(@"user pressed Button Cancel");
            // Any action can be performed here
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"user pressed Button Add Overlay on Image");
            
            // Identify which image is currently selected.
            CGFloat pageWidth = _m_imageScrollView.frame.size.width;
            int page = floor((_m_imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            UIImageView* v = _m_imageScrollView.subviews[page];
            UIImage* img = v.image;
            
            // Launch the sketch viewcontroller with the image as background
            // Launch the sketching view controller
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            SketchViewController * vc = (SketchViewController*)[storyboard instantiateViewControllerWithIdentifier:@"SketchViewController"];
           
            [vc setBackground:img];
            // Fancy Block command here.  Nice job, Jean.  It will go here when the image is updated in the other view controller.
            vc.somethingHappenedInModalVC = ^(UIImage *sketch) {
                
                // Add the sketch to the list of images
                [_m_images removeObjectAtIndex:_m_images.count -1];
                [_m_images addObject:sketch];
                [_m_images addObject:_m_placeholder];
                
                [self.view setNeedsLayout];
                [self.view layoutIfNeeded];
                [self archive];
                
            };
            
            
            [self presentViewController:vc animated:YES completion:nil];

        }
        else
        {
            NSLog(@"user pressed Button Delete");
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Delete Image?"
                                                           message: @"You cannot undo this."
                                                          delegate: self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"Delete",nil];
            [alert setTag:7];
            [alert show];
            
        }
    }
    
    else if (alertView.tag == 2) { // Audio Recording
        if (buttonIndex == 0)
        {
            NSLog(@"user pressed cancel");
            // Any action can be performed here
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"user pressed record audio");
            [self record];
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Audio Field Notes"
                                                           message: @"Recording..."
                                                          delegate: self
                                                 cancelButtonTitle:nil
                                                 otherButtonTitles:@"Stop",nil];
            [alert setTag:3];
            [alert show];
        }
        else
        {
           // The user pressed playback audio
            NSLog(@"user pressed playback audio recording");
            [self audioPlayback];
        }
    }
    
    else if (alertView.tag == 3) { // Stop Audio Recording
        if (buttonIndex == 0)
        {
            NSLog(@"user pressed stop audio recording");
            [self stopRecording];
        }
    }
    
    else if (alertView.tag == 5) { // GPS Location
        if (buttonIndex == 0)
        {
            NSLog(@"user pressed cancel");
        }
        else if (buttonIndex == 1)
        {
            NSLog(@"user pressed manual edit");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Modify GPS Location"
                                                            message:@"Enter new location:"
                                                           delegate:self
                                                  cancelButtonTitle:@"Done"
                                                  otherButtonTitles:nil];
            alert.alertViewStyle = UIAlertViewStylePlainTextInput;
            [alert textFieldAtIndex:0].text = _m_locationButton.titleLabel.text;
            
            [alert setTag:6];
            [alert show];
        }
        else if (buttonIndex == 2)
        {
            NSLog(@"user pressed current gps location");
            [self updateLocationButton];
        }
    }
    
    else if (alertView.tag == 6) { // GPS Location Manually entered
        if (buttonIndex == 0)
        {
            NSLog(@"user manually entered GPS location");
            [_m_locationButton setTitle:[alertView textFieldAtIndex:0].text forState:UIControlStateNormal];
        }
    }
    
    else if (alertView.tag == 7) { // Delete Image
        if (buttonIndex == 1)
        {
            NSLog(@"Deleting image");
            // Identify which image is currently selected.
            CGFloat pageWidth = _m_imageScrollView.frame.size.width;
            int page = floor((_m_imageScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
            
            if (page != _m_images.count-1)
            {
                [_m_images removeObjectAtIndex:page];
                [self.view setNeedsLayout];
                [self.view layoutIfNeeded];
            }
        }
    }
    
    else if (alertView.tag == 8)  // RFID Entered
    {
        if (buttonIndex == 1)
        {
            _sampleID = [alertView textFieldAtIndex:0].text;
            NSString* title = [[NSString alloc]initWithFormat:@"RFID: "];
            title = [title stringByAppendingString:_sampleID];
            [_RFID setTitle:title forState:UIControlStateNormal];
        }
    }
    
    else if (alertView.tag == 9)  // username and expedition title entered
    {
        if (buttonIndex == 0)
        {
            _username   = [alertView textFieldAtIndex:0].text;
            _expedition = [alertView textFieldAtIndex:1].text;
            
            // save username and expedition title to the preferences store
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            
            [userDefaults setObject:_username
                             forKey:@"username"];
            [userDefaults setObject:_expedition
                             forKey:@"expedition"];
            
            [userDefaults synchronize];
        }
    }
    
    else if (alertView.tag == 10) { // Take New Photo of Sample Alert View
        if (buttonIndex == 0)
        {
            NSLog(@"cancel");
            // Any action can be performed here
        }
        else if (buttonIndex <= 3)
        {
            NSLog(@"photo");
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            NSLog(@"sketch");
            
            // Launch the sketching view controller
            NSString * storyboardName = @"Main";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
            SketchViewController * vc = (SketchViewController*)[storyboard instantiateViewControllerWithIdentifier:@"SketchViewController"];
            
            // Fancy Block command here.  Nice job, Jean.  It will go here when the image is updated in the other view controller.
            vc.somethingHappenedInModalVC = ^(UIImage *sketch) {
                
                // Add the sketch to the list of images
                [_m_images removeObjectAtIndex:_m_images.count -1];
                [_m_images addObject:sketch];
                [_m_images addObject:_m_placeholder];
                [self.view setNeedsLayout];
                [self.view layoutIfNeeded];
                [self archive];
                
            };
            
            
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}


-(UIImage *)overlayImage:(UIImage*)backImage withFront:(UIImage *)frontImage
{
    UIGraphicsBeginImageContextWithOptions(backImage.size, NO, 0.0f);
    [backImage drawInRect:CGRectMake(0, 0, backImage.size.width, backImage.size.height)];
    [frontImage drawInRect:CGRectMake(0, backImage.size.height - frontImage.size.height, frontImage.size.width, frontImage.size.height)];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}


- (IBAction)RecordAudio:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Audio Field Notes"
                                                   message: @"What would you like to do?"
                                                  delegate: self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Record Audio",@"Playback Audio",nil];
    [alert setTag:2];
    [alert show];
}





- (IBAction)Delete:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Are you sure you want to delete this outcrop and all its samples?"
                                                   message: @"You cannot undo this."
                                                  delegate: self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Delete",nil];
    [alert setTag:4];
    [alert show];

}





- (IBAction)glossaryButtonPushed:(id)sender {
    
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    GlossaryViewController * vc = (GlossaryViewController*)[storyboard instantiateViewControllerWithIdentifier:@"GlossaryViewController"];
    
    
    NSMutableArray* entityNames = [[NSMutableArray alloc] init];
    
    // Load all outcropnames and samplenames in the array of entities
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSError *error;
    
    // Load all outcrops first
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"OutcropEntity" inManagedObjectContext:context]];
    
    NSMutableArray *predicateArray = [NSMutableArray array];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
    [predicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
    NSPredicate *compoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    [request setPredicate:compoundpred];
    
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error)
        NSLog(@"Whoops, couldn't fetch requests on OutcropEntity: %@", [error localizedDescription]);
    
    
    if (results.count > 0)
    {
        for (OutcropEntity* o in results)
        {
            [entityNames addObject:o.outcropName];
            
            // Now load all samples
            NSFetchRequest *samplerequest = [[NSFetchRequest alloc] init];
            [samplerequest setEntity:[NSEntityDescription entityForName:@"SampleEntity" inManagedObjectContext:context]];
            
            NSMutableArray* samplePredicateArray = [[NSMutableArray alloc] init];
            [samplePredicateArray addObject:[NSPredicate predicateWithFormat:@"expedition == %@", _expedition]];
            [samplePredicateArray addObject:[NSPredicate predicateWithFormat:@"username == %@", _username]];
            [samplePredicateArray addObject:[NSPredicate predicateWithFormat:@"sampleParentOutcropName == %@", o.outcropName]];
            NSCompoundPredicate* samplecompoundpred = [NSCompoundPredicate andPredicateWithSubpredicates:samplePredicateArray];
            [samplerequest setPredicate:samplecompoundpred];
            
            NSArray* sampleresults = [context executeFetchRequest:samplerequest error:&error];
            if (error)
                NSLog(@"Whoops, couldn't fetch requests on SampleEntity: %@", [error localizedDescription]);
            
            
            if (sampleresults.count > 0)
            {
                for (SampleEntity* s in sampleresults)
                {
                    NSString* sampleName = s.sampleName;
                    if (s.sampleRFID != nil)
                    {
                        sampleName = [sampleName stringByAppendingString:@"  RFID: "];
                        sampleName = [sampleName stringByAppendingString:s.sampleRFID];
                    }
                    [entityNames addObject:s.sampleName];
                }
            }
        }
    }
    
    [vc setListOfEntities:entityNames];
    
    
    [self presentViewController:vc animated:YES completion:nil];
    
    
}







// Camera delegate
    -(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_m_images removeObjectAtIndex:_m_images.count -1];
    [_m_images addObject:chosenImage];
    [_m_images addObject:_m_placeholder];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    [self archive];

    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

// When camera was cancelled

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

// Compass heading and current location
-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
{
}



-(void) updateHeadingButton
{
    NSString* mag = [NSString stringWithFormat:@"%f", self.locationManager.heading.magneticHeading];
    NSString* tru = [NSString stringWithFormat:@"%f", self.locationManager.heading.trueHeading];
    
    _m_currentMagHeading = @"Magnetic Heading: ";
    _m_currentTrueHeading = @"True Heading: ";
    
    _m_currentMagHeading = [_m_currentMagHeading stringByAppendingString:mag];
    
    _m_currentTrueHeading = [_m_currentTrueHeading stringByAppendingString:tru];
    
    
    [_magHeadingButton setTitle:_m_currentMagHeading forState:UIControlStateNormal];
    [_trueHeadingButton setTitle:_m_currentTrueHeading forState:UIControlStateNormal];
    
    [self archive];
}


- (IBAction)LocationButton:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"Refine Location?"
                                                   message: @"You cannot undo this."
                                                  delegate: self
                                         cancelButtonTitle:@"Cancel"
                                         otherButtonTitles:@"Manual Edit",@"Current Location",nil];
    [alert setTag:5];
    [alert show];
}


- (IBAction)HeadingButton:(id)sender {
    [self updateHeadingButton];
}



////// START AUDIO RECORDING IMPLEMENTATION //////

- (NSString *) dateString
{
    // return a formatted string for a file name
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"ddMMMYY_hhmmssa";
    return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".m4a"];
}

- (BOOL) record
{
    // Stop the audio player before recording
    if (_player.playing) {
        [_player stop];
    }
    
    if (!_recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [_recorder record];
        
    } else {
        
        // Pause recording
        [_recorder pause];
    }
    return true;
}



- (void) stopRecording
{
    [_recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    [self archive];
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
}

-(void) audioPlayback
{
    if (!_recorder.recording){
        NSString* strURL = [_recorder.url absoluteString];
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:_recorder.url error:nil];
        [_player setDelegate:self];
        [_player play];
    }
}

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Done"
                                                    message: @"Finish playing the recording!"
                                                   delegate: nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

////// END AUDIO RECORDING IMPLEMENTATION //////


// FOR CORE DATA //

// For core-data

- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DataModel" withExtension:@"momd"];

    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ModelCoreData.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// END CORE DATA //

@end
