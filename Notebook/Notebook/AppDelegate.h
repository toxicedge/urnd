//
//  AppDelegate.h
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

