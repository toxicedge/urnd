//
//  BuddyPins+CoreDataProperties.m
//  Notebook
//
//  Created by Student on 2016-03-25.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BuddyPins+CoreDataProperties.h"

@implementation BuddyPins (CoreDataProperties)

@dynamic outcropName;
@dynamic latitude;
@dynamic longtitude;

@end
