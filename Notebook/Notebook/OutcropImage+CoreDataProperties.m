//
//  OutcropImage+CoreDataProperties.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "OutcropImage+CoreDataProperties.h"

@implementation OutcropImage (CoreDataProperties)

@dynamic outcropImage;
@dynamic outcrop;

@end
