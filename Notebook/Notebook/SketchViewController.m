//
//  SketchViewController.m
//  Notebook
//
//  Created by Karen Filion on 2016-01-23.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "SketchViewController.h"
#import "FinalAlgView.h"
#import "ReaderViewController.h"

@interface SketchViewController ()

@end

@implementation SketchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) setBackground:(UIImage*)img
{
    // Get a pointer to the view
    FinalAlgView* v = (FinalAlgView*)self.view;
    [v setBackgound:img];
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}


- (IBAction)infoButtonPressed:(id)sender {
    // Launch info pdf scrolling view controller
    
    NSString *phrase = nil; // Document password (for unlocking most encrypted PDF files)
    
    NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    
    NSString *filePath = [pdfs firstObject]; assert(filePath != nil); // Path to first PDF file
    
    ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:phrase];
    
    if (document != nil) // Must have a valid ReaderDocument object in order to proceed with things
    {
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        
        [self presentViewController:readerViewController animated:YES completion:nil];
    }

}

- (IBAction)Done:(id)sender {
    
    [_saveButton setHidden:true];
    [_penButton setHidden:true];
    [_sharpieButton setHidden:true];
    [_eraserButton setHidden:true];
    [_textButton setHidden:true];
    [_clearScreenButton setHidden:true];
    [_colorsButton setHidden:true];
    [_info setHidden:true];
    
    // Save what is in the view as a UIImage
    UIView* v = self.view;
    ((FinalAlgView*)v).grid = false;
    [(FinalAlgView*)v setNeedsDisplay];
    
    // Add it to the list of images of this outcrop, sample, or subsample.
    UIImage* sketch = [self imageWithView:v];
    
    self.somethingHappenedInModalVC(sketch);
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
