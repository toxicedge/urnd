//
//  FinalAlgView.h
//  VariableStrokeWidthTut
//
//  Created by A Jean Filion on 24/01/2016.
//  Copyright (c) 2016 JF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VBColorPicker.h"

@interface FinalAlgView : UIView <VBColorPickerDelegate>

@property (nonatomic, strong) VBColorPicker *cPicker;
@property UIColor* color;

@property bool grid;

-(void)setBackgound:(UIImage*)img;

@end
