//
//  Entity.h
//  Notebook
//
//  Created by Karen Filion on 2016-02-28.
//  Copyright © 2016 Western University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entity : NSObject

@property (nonatomic, copy) NSString *entity;


@end
