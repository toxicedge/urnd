//
//  SampleEntity+CoreDataProperties.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-27.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SampleEntity+CoreDataProperties.h"

@implementation SampleEntity (CoreDataProperties)

@dynamic date;
@dynamic expedition;
@dynamic sampleAudioRecording;
@dynamic sampleDateTimeOfCollection;
@dynamic sampleHeadingMagnetic;
@dynamic sampleHeadingTrue;
@dynamic sampleImages;
@dynamic sampleLocation;
@dynamic sampleName;
@dynamic sampleNotes;
@dynamic sampleParentOutcropName;
@dynamic samplePixelCoordinatesOnOutcropImages;
@dynamic sampleRFID;
@dynamic username;
@dynamic images;
@dynamic outcrop;
@dynamic subsamples;

@end
