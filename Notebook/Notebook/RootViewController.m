//
//  RootViewController.m
//  Notebook
//
//  Created by Karen Filion on 2016-01-07.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "RootViewController.h"
#import "ModelController.h"
#import "DataViewController.h"
#import "OutcropEntity.h"

@interface RootViewController ()
@property (nonatomic,readonly, strong) ModelController *modelController;
@end

@implementation RootViewController

@synthesize modelController = _modelController;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    // Configure the page view controller and add it as a child view controller.
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    
    _pageNumAfterGlossary = 0;

    DataViewController *startingViewController = [self.modelController viewControllerAtIndex:0 storyboard:self.storyboard];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    self.pageViewController.dataSource = self.modelController;

    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];

    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.view.bounds;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);
    }
    self.pageViewController.view.frame = pageViewRect;

    [self.pageViewController didMoveToParentViewController:self];

    // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
    self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(autoPage:) name:@"H2RAutoPage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NewOutcrop:) name:@"NewOutcrop" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NewSample:) name:@"NewSample" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NewSubSample:) name:@"NewSubSample" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)autoPage: (NSNotification *)notification
{
    NSNumber *index = [[notification userInfo] valueForKey:@"index"];
    int idx = index.intValue;
    
    //get current index of current page
    NSUInteger currentPage = [self.modelController indexOfViewController:[self.pageViewController.viewControllers objectAtIndex:0]];
    // this is the next page nil mean we have reach the end
    DataViewController *targetPageViewController = [self.modelController viewControllerAtIndex:idx storyboard:self.storyboard];
    if (targetPageViewController) {
        //put it(or them if in landscape view) in an array
        NSArray *viewControllers = [NSArray arrayWithObjects:targetPageViewController, nil];
        //add page view
        [self.pageViewController setViewControllers:viewControllers  direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    }
}

- (void)NewOutcrop: (NSNotification *)notification
{
    DataViewController *startingViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    [_modelController AddOutcrop:startingViewController];
    
    // Jump to the page with the outcrop
    int count = [_modelController getNumberOfPages] - 1;
    NSNumber* index = [[NSNumber alloc] initWithInt:count];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:index forKey:@"index"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"H2RAutoPage" object:nil userInfo:dictionary];
}

-(NSString*)GetMyParentOutcropName:(NSString*)myName
{
    return [_modelController GetMyParentOutcropName:myName];
}

-(void)setSplLocations:(NSMutableArray*)locations;
{
    _sampleLocations = locations;
}

-(NSMutableArray*)getSplLocations;
{
    return _sampleLocations;
}

-(int) getNumberOfPages
{
    return [_modelController getNumberOfPages];
}

-(void) loadOutcrop:(OutcropEntity*) o
{
    [_modelController LoadOutcrop:o];
}

-(void) loadSample:(SampleEntity*) s
{
    [_modelController LoadSample:s];
}

- (void)NewSample: (NSNotification *)notification
{
    NSString *parentOutcropName = [[notification userInfo] valueForKey:@"sampleParentOutcropName"];
    
     DataViewController *startingViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    [_modelController AddSample:startingViewController];
    
    // Jump to the page with the sample
    int count = [_modelController getNumberOfPages] - 1;
    NSNumber* index = [[NSNumber alloc] initWithInt:count];
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObject:index forKey:@"index"];
    [dictionary setObject:parentOutcropName forKey:@"sampleParentOutcropName"];  // This allows us to know what outcrop this sample belongs to.
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"H2RAutoPage" object:nil userInfo:dictionary];
}

-(void)NewSubSample: (NSNotification*)notification
{
    DataViewController *startingViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    [_modelController AddSubSample:startingViewController];
    
    // Jump to the page with the outcrop
    int count = [_modelController getNumberOfPages] - 1;
    NSNumber* index = [[NSNumber alloc] initWithInt:count];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:index forKey:@"index"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"H2RAutoPage" object:nil userInfo:dictionary];
}

- (ModelController *)modelController {
    // Return the model controller object, creating it if necessary.
    // In more complex implementations, the model controller may be passed to the view controller.
    if (!_modelController) {
        _modelController = [[ModelController alloc] init];
    }
    return _modelController;
}

#pragma mark - UIPageViewController delegate methods

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    if (UIInterfaceOrientationIsPortrait(orientation) || ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)) {
        // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to YES, so set it to NO here.
        
        UIViewController *currentViewController = self.pageViewController.viewControllers[0];
        NSArray *viewControllers = @[currentViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        
        self.pageViewController.doubleSided = NO;
        return UIPageViewControllerSpineLocationMin;
    }

    // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
    DataViewController *currentViewController = self.pageViewController.viewControllers[0];
    NSArray *viewControllers = nil;

    NSUInteger indexOfCurrentViewController = [self.modelController indexOfViewController:currentViewController];
    if (indexOfCurrentViewController == 0 || indexOfCurrentViewController % 2 == 0) {
        UIViewController *nextViewController = [self.modelController pageViewController:self.pageViewController viewControllerAfterViewController:currentViewController];
        viewControllers = @[currentViewController, nextViewController];
    } else {
        UIViewController *previousViewController = [self.modelController pageViewController:self.pageViewController viewControllerBeforeViewController:currentViewController];
        viewControllers = @[previousViewController, currentViewController];
    }
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];


    return UIPageViewControllerSpineLocationMid;
}

-(void)setPageOnDismissingGlossary:(int)num
{
    _pageNumAfterGlossary = num;
}

-(int)getPageOnDismissingGlossary
{
    return _pageNumAfterGlossary;
}


@end
