//
//  BuddyPins+CoreDataProperties.h
//  Notebook
//
//  Created by Student on 2016-03-25.
//  Copyright © 2016 Western University. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BuddyPins.h"

NS_ASSUME_NONNULL_BEGIN

@interface BuddyPins (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *outcropName;
@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSNumber *longtitude;

@end

NS_ASSUME_NONNULL_END
