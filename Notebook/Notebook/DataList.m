//
//  DataList.m
//  Notebook
//
//  Created by Student on 2016-03-15.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "DataList.h"

@implementation DataList

@synthesize longitude;
@synthesize latitude;
@synthesize username;
@synthesize expiditionName;
@synthesize date;

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.longitude = [decoder decodeObjectForKey:@"longtitude"];
    self.latitude = [decoder decodeObjectForKey:@"latitude"];
    self.username = [decoder decodeObjectForKey:@"username"];
    self.expiditionName = [decoder decodeObjectForKey:@"expeditionName"];
    self.date = [decoder decodeObjectForKey:@"date"];
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        self.longitude = 0;
        self.latitude = 0;
        self.username = 0;
        self.expiditionName = 0;
        self.date = 0;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.longitude forKey:@"longtitude"];
    [encoder encodeObject:self.latitude forKey:@"latitude"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.expiditionName forKey:@"expeditionName"];
    [encoder encodeObject:self.date forKey:@"expeditionName"];
  
}

- (NSMutableData*) generateUID
{
    NSMutableData *uid;
    double lon = [self.longitude doubleValue];
    double lat = [self.latitude doubleValue];
    uid = [NSMutableData dataWithBytes:&lon length:sizeof(double)];
    [uid appendBytes:&lat length:sizeof(double)];
    
    return uid;
}
@end
