//
//  EntityTableViewCell.m
//  Notebook
//
//  Created by Karen Filion on 2016-02-28.
//  Copyright © 2016 Western University. All rights reserved.
//

#import "EntityTableViewCell.h"

@implementation EntityTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
